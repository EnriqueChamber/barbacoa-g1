== Lista de bebidas

// Separadas en con/sin alcohol
// Ordenadas por orden alfabético

=== Con alcohol

* Cerveza
* Ron
* Tinto de verano
* Whisky
* Rebujito
* Vodka

=== Sin alcohol

* 7up
* Bitter Kas
* Agua
* Cerveza sin
* CocaCola
* Agua con gas
* Dr Pepper
* Zumo de melocotón
* Zumo de piña
* Red Bull
